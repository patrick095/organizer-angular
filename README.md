# OrganizerAngular

[![CI Angular](https://github.com/patrick095/organizer-angular/actions/workflows/main.yml/badge.svg)](https://github.com/patrick095/organizer-angular/actions/workflows/main.yml)
[![Lint](https://github.com/patrick095/organizer-angular/actions/workflows/lint.yml/badge.svg)](https://github.com/patrick095/organizer-angular/actions/workflows/lint.yml)

## Cobertura dos testes
| Statements                  | Branches                | Functions                 | Lines             |
| --------------------------- | ----------------------- | ------------------------- | ----------------- |
| ![Statements](https://img.shields.io/badge/statements-58.43%25-red.svg?style=flat) | ![Branches](https://img.shields.io/badge/branches-40%25-red.svg?style=flat) | ![Functions](https://img.shields.io/badge/functions-48.95%25-red.svg?style=flat) | ![Lines](https://img.shields.io/badge/lines-53.45%25-red.svg?style=flat) |

### Projeto que estou refazendo em Angular de um organizador.

#### P.S Ainda sem nome kk

Hospedado na Vercel
https://organizer-angular.vercel.app/
